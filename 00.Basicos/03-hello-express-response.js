'use strict'

var express = require('express'),
    app = express()

app
    .get('/', (req, res) => {
    //res.end('<h1>Hola mundo desde Express</h1>')
    res.send('<h1>Hola mundo desde Express</h1>')
    })
    .get('/fatm', (req, res) => {
    //res.end('<h1>Hola mundo desde Express</h1>')
    //res.send('<h1>Bienvenidos a fatm</h1>')
    res.redirect(301, 'http://www.elbraserodedonpedro.com/main/main.html')
    })
    .get('/json', (req, res) => {
        //http://localhost:3000/json
        res.json({
            name: "felix",
            age: 50,
            correo: "futu@hotmail.com"
        })
    })
    .get('/render', (req, res) => {
        res.render(`${__dirname}/assets/index.html`)
        })
    .listen(3000)

console.log('Iniciando Express en el puerto 3000')