'user strict'

var express = require('express'),
    app = express()

app
    .get('/', (req, res) => {
    res.end('<h1>Hola mundo desde Express :)</h1>')
    })
    //http://localhost:3000/user/19-Felix-50
    .get('/user/:id-:name-:age', (req, res) => {
        res.end(`<h1>${req.params.name}, bienvenid@ a Express :)
                tu id es ${req.params.id} </b>
                y tienes ${req.params.age} tacos</b>
                </h1>`)
        })
    //http://localhost:3000/search
    //http://localhost:3000/search?s=perros
    .get('/search', (req, res) => {
        res.end(`
            <h1>
                Bienvenido a Express, los resultados de tu busqueda son:
                <mark>${req.query.s}</mark>
            </h1>
        `)    
    } )

    .listen(3000)

console.log('Iniciando Express en el puerto 3000')