'use strict'

var createError = require('http-errors'),
    express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    indexRouter = require('./routes/index'),
    usersRouter = require('./routes/users'),
    app = express()

// view engine setup
app
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'jade')
  .use(logger('dev'))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded( { extended: false }))
  .use(cookieParser())
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use(express.static(path.join(__dirname, 'public')))
  .use('/', indexRouter)
  .use('/users', usersRouter)
  // catch 404 and forward to error handler
  .use(function(req, res, next) {
        next(createError(404))
      })
  // error handler
  .use(function(err, req, res, next) {
  
      // set locals, only providing error in development
      res
        .locals.message = err.message
        .locals.error = req.app.get('env') === 'development' ? err : {}
        // render the error page
        .status(err.status || 500)
        .render('error')
      })

module.exports = app
